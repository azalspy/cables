from azalspy.health.helpers import *

###################################################################################################

class TvSerie(BaseItem):
    broad = scrapy.Field()
    alias = scrapy.Field()
    title = scrapy.Field()

    plots = scrapy.Field()
    birth = scrapy.Field()
    state = scrapy.Field()

    genre = scrapy.Field(default=[])
    cover = scrapy.Field(default=[])
    names = scrapy.Field(default=[])

    @property
    def media(self):
        yield "%(alias)s.jpg" % self,self['cover']

    MEDIA_PATH = ['gogoanime.io','cover']

    DISK_FOLDER = ['gogoanime.io','items']
    DISK_TARGET = "%(alias)s.json"

class TvEpisode(BaseItem):
    broad = scrapy.Field()
    serie = scrapy.Field()
    alias = scrapy.Field()

    title = scrapy.Field()
    infos = scrapy.Field(default={})
    categ = scrapy.Field()

    video = scrapy.Field(default={})
    links = scrapy.Field(default=[])

    DISK_FOLDER = ['gogoanime.io','links']
    DISK_TARGET = "%(alias)s.json"

