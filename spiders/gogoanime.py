#-*- coding: utf-8 -*-

from azalspy.cables.utils import *

BASE_LINK = 'https://www10.gogoanime.io'

####################################################################################

class GogoAnimeSpider(BaseSpider):
    name = "gogoanime"
    start_urls = [
        BASE_LINK + '/',
    ]
    CATEGORY_LINK = BASE_LINK + '/category/%s'

    def parse(self, response):
        for link in [
            '/anime-movies.html',
            '/popular.html',
            '/new-season.html',
        ]:
            yield scrapy.Request(BASE_LINK + link, callback=self.parse_listing)

        yield scrapy.Request(BASE_LINK + '/anime-list.html', callback=self.parse_alphabet)

        for link in response.xpath('//nav[@class="menu_series cron"]//a/@href').extract():
            yield scrapy.Request(BASE_LINK + link, callback=self.parse_category)

        for link in response.xpath('//div[@class="added_series_body popular"]//a/@href').extract():
            yield scrapy.Request(BASE_LINK + link, callback=self.parse_video)

        for link in response.xpath('//div[@class="added_series_body final"]//a/@href').extract():
            yield scrapy.Request(BASE_LINK + link, callback=self.parse_category)

        for link in response.xpath('//nav[@class="menu_series genre right"]//a/@href').extract():
            yield scrapy.Request(urljoin(BASE_LINK, link), callback=self.parse_listing)

    def parse_listing(self, response):
        for unit in response.xpath('//ul[@class="items"]/li'):
            link = unit.xpath('.//a/@href').extract()[0]

            yield scrapy.Request(BASE_LINK + link, callback=self.parse_category)

            #badge = TvBadger(broad=self.name)

            #entry['title'] = unit.xpath('./a//text()').extract()
            #entry['cover'] = unit.xpath('./img/@src').extract()
            #entry['cover'] = unit.xpath('./p[@class="released"]/text()').extract()

            #yield entry

        for link in response.xpath('//ul[@class="pagination-list"]//a/@href').extract():
            yield scrapy.Request(urljoin(response.url, link), callback=self.parse_listing)

    def parse_alphabet(self, response):
        for link in response.xpath('//li[@class="first-char"]//a/@href').extract():
            yield scrapy.Request(BASE_LINK + link, callback=self.parse_alphabet)

        for link in response.xpath('//ul[@class="listing"]//a/@href').extract():
            yield scrapy.Request(BASE_LINK + link, callback=self.parse_category)

        for link in response.xpath('//ul[@class="pagination-list"]//a/@href').extract():
            yield scrapy.Request(urljoin(response.url, link), callback=self.parse_alphabet)

    def parse_category(self, response):
        entry = TvSerie(broad=self.name)

        entry['alias'] = response.url.replace(BASE_LINK,'').replace('/category/','')
        entry['title'] = response.xpath('//div[@class="anime_info_body_bg"]//h1/text()').extract()[0]

        entry['plots'] = response.xpath('//p[@class="type"][2]/text()').extract()
        entry['genre'] = response.xpath('//p[@class="type"][3]/a/@title').extract()
        entry['cover'] = response.xpath('//div[@class="anime_info_body_bg"]//img/@src').extract()[0]

        entry['birth'] = response.xpath('//p[@class="type"][4]/text()').extract()[0]
        entry['state'] = response.xpath('//p[@class="type"][5]/text()').extract()[0]
        entry['names'] = response.xpath('//p[@class="type"][6]/text()').extract()

        yield entry

        #https://ajax.apimovie.xyz/ajax/load-list-episode?ep_start=0&ep_end=25&id=1764&default_ep=0&alias=steinsgate

        for link in response.xpath('//ul[@id="episode_related"]//a/@href').extract():
            yield scrapy.Request(link, callback=self.parse_video)

    def parse_video(self, response):
        entry = TvSerie(broad=self.name)

        entry['alias'] = response.xpath('//p[@class="type"][6]/text()').extract()
        entry['title'] = response.xpath('//div[@class="anime_video_body"]//h1/text()').extract()

        entry['categ'] = response.xpath('//div[@class="anime_video_body_cate"]/a[1]').extract()
        entry['serie'] = response.xpath('//div[@class="anime-info"]/a[1]').extract()

        yield scrapy.Request(self.CATEGORY_LINK % link, callback=entry.categ)
        yield scrapy.Request(self.CATEGORY_LINK % link, callback=entry.categ)

        for item in response.xpath('//div[@©lass="anime_muti_link"]//a[@data-video]'):
            key = item.xpath('./text()').extract()[0].replace('Choose this server','')

            entry['video'][key] = item.xpath('./@data-video').extract()[0]

        #entry.links = response.xpath('//p[@class="type"][5]/text()').extract()

        for link in response.xpath('//nav[@class="menu_recent"]//a/@href').extract():
            yield scrapy.Request(urljoin(BASE_LINK, link), callback=self.parse_video)

        yield entry

